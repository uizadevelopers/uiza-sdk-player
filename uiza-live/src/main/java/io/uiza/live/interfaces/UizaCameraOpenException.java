package io.uiza.live.interfaces;


public class UizaCameraOpenException extends RuntimeException {
    public UizaCameraOpenException(String message) {
        super(message);
    }
}
