package io.uiza.live.interfaces

interface CameraChangeListener{
    fun onCameraChange(isFrontCamera: Boolean)
}